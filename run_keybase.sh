#!/bin/bash

trap 'fusermount -u ${KEYBASE_KBFS_MOUNT}' SIGINT SIGTERM

# run keybase
run_keybase

# Wait for keybase to be ready?
for i in 0 1 2 3 4 5 6 7 8 9 ; do
  if [ -e /home/`whoami`/.config/keybase/keybased.sock ] ; then
    echo "keybase is up" >&2
    break
  fi
  echo "keybase is NOT up $i" >&2
  if [ $i = 9 ] ; then
    echo "keybase did NOT come up" >&2
    exit 1
  fi
  sleep 1
done

# Login
keybase oneshot

keybase config set mountdir "${KEYBASE_KBFS_MOUNT}"

kbfsfuse &

pid="$!"

while true; do
  sleep 1
done

